import numpy as np
from flask import Flask, request, jsonify, render_template
import pickle
import tensorflow as tf
import numpy as np
from tensorflow import keras

app = Flask(__name__)
new_model = keras.models.load_model('tea_predict.h5')

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/predict',methods=['POST'])
def predict():
    '''
    For rendering results on HTML GUI
    '''
    
    new_model.summary()

    #v =np.array([[19.014286,	30.396429,	204.3	,6.6,	6.6	,12	,1.16	,20	,21]])
    v2 =np.array([[float(x) for x in request.form.values()]])
    
    prediction = new_model.predict(v2)
    output = prediction[0]
    print(output[0])

    return render_template('index.html', prediction_text='Valuation Price should be Rs: {0:.2f}'.format(output[0]))

@app.route('/predict_api',methods=['POST'])
def predict_api():
    '''
    For direct API calls trought request
    '''
    v3 =np.array([[float(x) for x in request.form.values()]])
    
    prediction = new_model.predict(v3)
    output = prediction[0]
    return jsonify(output)

if __name__ == "__main__":
    app.run(debug=True)